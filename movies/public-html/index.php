<?php
chdir(__DIR__);
require_once('./loader.php');
spl_autoload_register(function ($class) {
    $path = explode('\\', $class);
    if (!in_array($path[0], ['classes', 'views', 'models'])) {
        throw new \Exception('Access violation');
    }
    $filename = './core/' . join('/', $path) . '.php';
    if (!file_exists($filename)) {
        throw new \Exception('Class not ' . $class . ' found');
    }

    require_once($filename);

});


\classes\config::init();
$app = \classes\application::app();
$router = new \classes\router();
$router->get('/', function () {
    \classes\template::render('base', '', []);
});

$router->post('/ajax', function ($params) {
    switch ($params['post']['method']) {
        case 'auth_get_token':
            {
                \classes\authserver::getAccessToken($params['post']);
                break;
            }
        case 'mov_add_movie':
            {
                \classes\movies::addMovie($params['post']);
                break;
            }
        case 'mov_add_fav':
            {
                \classes\movies::addToFav($params['post']);
                break;
            }
        case 'mov_get_list':
            {
                \classes\movies::listMovies($params['post']);
                break;
            }

    }
});
