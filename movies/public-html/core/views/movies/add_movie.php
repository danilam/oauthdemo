<div id="mov1" class="cltable">
    <h3>Add Movie</h3>
    <div class="row">
        <div class="col col-xs-6"><input class="form-control" type="text" name="title" placeholder="Title">
        </div>
        <div class="col col-xs-6"><input class="form-control" type="text" name="year" placeholder="Year"></div>
    </div>
    <div>&nbsp;</div>
    <div class="row">
        <div class="col col-xs-10">
            <textarea name="description" placeholder="Description" class="form-control"></textarea>
        </div>
        <div class="col col-xs-2">
            <button class="btn btn-success"><span>Add movie</span></button>
        </div>
    </div>
    <div>&nbsp;</div>
    <div class="row">
        <div class="col col-xs-12">Result: <textarea readonly class="result-raw form-control"></textarea></div>
    </div>
</div>