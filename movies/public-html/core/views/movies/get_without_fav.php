<div id="mov3" class="cltable">
    <h3>Get Movies list</h3>
    <button class="btn btn-success"><span>Get movies</span></button>
    <div class="row">
        <div class="col col-xs-12">Result: <textarea readonly class="result-raw form-control"></textarea></div>
    </div>
        <div class="table">
        	<table class="table table-hover" style="font-size:13px;">
        		<thead>
        			<tr>
        				<th>ID</th>
        				<th>Title</th>
        				<th>Description</th>
        				<th>Year</th>
        				<th>Added by</th>
        				<th>Added date</th>
        			</tr>
        		</thead>
        		<tbody id="result_table"></tbody>
        	</table>
        </div>
</div>