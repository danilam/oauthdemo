<div id="mov2" class="cltable">
    <h3>Add To Favorite</h3>
    <div class="row">
        <div class="col col-xs-10"><input class="form-control" type="text" name="id" placeholder="Movie ID"></div>
        <div class="col col-xs-2">
            <button class="btn btn-success"><span>Add movie</span></button>
        </div>
    </div>
    <div>&nbsp;</div>
    <div class="row">
        <div class="col col-xs-12">Result: <textarea readonly class="result-raw form-control"></textarea></div>
    </div>
</div>