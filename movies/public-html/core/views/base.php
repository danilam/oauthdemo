<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="images/jquery-3.3.1.min.js"></script>
    <script src="images/jquery-ui.js"></script>
    <script src="images/bootstrap.min.js"></script>
    <script src="images/app.js"></script>
    <link rel="stylesheet" href="images/jquery-ui.css">
    <link rel="stylesheet" href="images/jquery-ui.theme.css">
    <link rel="stylesheet" href="images/jquery-ui.structure.css">
    <link rel="stylesheet" href="images/bootstrap.css">
    <link rel="stylesheet" href="images/bootstrap-theme.css">
    <link rel="stylesheet" href="images/style.css">
</head>
<body>
<div class="container base1">
    <?php
    if ($include) {
        require($include);
    }
    ?>
    <h1>Auth server methods</h1>

    <div class="row">
        <div class="col col-xs-3 legend-class">
            1. Необходимо реализовать отдельное приложение для работы с данными пользователей.<br> <br>
            Сущность “Пользователь”:<br>
            логин;<br>
            пароль.<br>
            <br>
            Реализовать endpoint Oauth2 аутентификации для пользователя с использованием логина и пароля.
        </div>
        <div class="col col-xs-9">
            <?php \classes\template::render('auth/get_token'); ?>
        </div>
    </div>


    <div class="row">
        <div class="col col-xs-3 legend-class">
            2. Необходимо реализовать отдельное приложение для работы с данными о фильмах.<br>
            Сущность “Фильм”:<br>
            заголовок;<br>
            описание;<br>
            год выхода фильма;<br>
            временная метка добавления фильма в базу;<br>
            пользователь, который добавил информацию о фильме.<br> <br>



            endpoint добавления нового фильма;
        </div>
        <div class="col col-xs-9">
            <?php \classes\template::render('movies/add_movie'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col col-xs-3 legend-class">
            endpoint получение перечня фильмов для пользователя, которые исключают фильмы добавленные в избранное.
        </div>
        <div class="col col-xs-9">
            <?php \classes\template::render('movies/get_without_fav'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col col-xs-3 legend-class">
            endpoint добавления в избранное выбранный фильм текущему пользователю;
        </div>
        <div class="col col-xs-9">

            <?php \classes\template::render('movies/add_fav'); ?>
        </div>

    </div>
</div>
</body>
</html>