<div id="ex1" class="cltable">
    <h3>Get token</h3>
    <div class="span-links">
        <span>test1;test1pass</span>
        &nbsp; <span>test2;test2pass</span>
        &nbsp; <span>test3;test3pass</span>
        &nbsp; <span>test4;test4pass</span>
        &nbsp; <span>test5;test5pass</span>
        &nbsp; <span>newtest;passnewtest</span>
        &nbsp; <span>newuser;anewpass</span>
    </div>
    <br>
    <div class="row">
        <div class="col col-xs-4"><input type="text" class="form-control" name="username" placeholder="username"></div>
        <div class="col col-xs-4"><input type="text" class="form-control" name="password" placeholder="password"></div>
        <div class="col col-xs-4">
            <button type="button" class="btn btn-success"><span>get token</span></button>
        </div>
    </div>
    <div>&nbsp;</div>
    <div class="row">
        <div class="col col-xs-6">Token: <input type="text" readonly class="result-text form-control" id="token"
                                                placeholder="result"></div>
        <div class="col col-xs-6">Raw: <textarea readonly class="result-raw form-control"></textarea></div>
    </div>
</div>

