<?php

namespace classes;

class movies
{
    static function addToFav($params)
    {
        if (!isset($params['token']) || !$auth_id = application::app()->getAuthId($params)) {
            application::app()->returnJson('error', 'Please, sign in first');
        }

        $movie_id = intval($params['movie_id']);
        if (!$movie_id) {
            application::app()->returnJson('error', 'No movie ID provided');
        }
        $res = application::app()->db()->query('select count(*) as cnt from movies where movie_id=' . $movie_id);
        $cnt = $res->fetch(\PDO::FETCH_ASSOC);
        if (!$cnt['cnt']) {
            application::app()->returnJson('error', 'Movie not found');
        }
        $res = application::app()->db()->query('INSERT INTO fav(user_id, movie_id) VALUES("' . $auth_id . '", "' . $movie_id . '")');
        if ($res) {
            application::app()->returnJson('ok', 'ok');
        } else {
            application::app()->returnJson('error', application::app()->db()->errorInfo());
        }
    }

    static function listMovies($params)
    {
        if (!isset($params['token']) || !$auth_id = application::app()->getAuthId($params)) {
            application::app()->returnJson('error', 'Please, sign in first');
        }

        $res = application::app()->db()->query('
            
            select m.*, f.user_id as uid 
            from movies m 
            left join fav f
            ON m.movie_id=f.movie_id
            AND f.user_id=' . $auth_id . '
            GROUP BY m.movie_id, f.user_id
            HAVING uid IS NULL
            ');

        $result = $res->fetchAll(\PDO::FETCH_ASSOC);
        application::app()->returnJson('ok', 'ok', $result);
    }


    static function addMovie($params)
    {

        if (!isset($params['token']) || !$auth_id = application::app()->getAuthId($params)) {
            application::app()->returnJson('error', 'Please, sign in first');
        }


        $title = (isset($params['title'])) ? htmlspecialchars(trim($params['title'])) : '';
        $description = (isset($params['description'])) ? htmlspecialchars(trim($params['description'])) : '';
        $year = (isset($params['year'])) ? intval($params['year']) : 0;

        if (!$title || !$description || !$year) {
            application::app()->returnJson('error', 'Please, fill all fields');
        }

        $res = application::app()->db()->query('INSERT INTO movies (title, description, created_at, added_at, added_by)
        VALUES ("' . $title . '", "' . $description . '", "' . $year . '", "' . date('Y-m-d H:i:s') . '", "' . $auth_id . '")');

        if (!$res) {
            application::app()->returnJson('error', application::app()->db()->errorInfo());

        } else {
            $id = application::app()->db()->lastInsertId();
            application::app()->returnJson('ok', 'success', ['id' => $id]);
        }

    }
}