<?php

namespace classes;


class authserver
{

    static function getAccessToken($request)
    {
        unset($_SESSION['token']);
        unset($_SESSION['username']);
        try {
            $token = application::app()->authProvider->getAccessToken('password', ['username' => $request['username'], 'password' => $request['password']]);

            $_SESSION['token'] = $token->getToken();
            $_SESSION['username'] = $request['username'];
            application::app()->returnJson('ok', 'success', ['token' => $token]);
        } catch (\Exception $e) {
            application::app()->returnJson('error', 'error', ['data' => $e->getMessage()]);
        }
    }
}