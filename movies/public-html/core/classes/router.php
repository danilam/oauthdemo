<?php

namespace classes;


class router
{
    var $method = '';
    var $uri = '';
    var $params = [
        'get' => [],
        'post' => []
    ];

    function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->params['get'] = $_GET;
        $this->params['post'] = $_POST;
        $this->uri = $_SERVER['REQUEST_URI'];
    }

    function get($path, $closure)
    {
        if ($this->method != 'GET' || $this->uri != $path) {
            return;
        }
        $closure($this->params);
        exit(0);
    }

    function post($path, $closure)
    {
        if ($this->method != 'POST' || $this->uri != $path) {
            return;
        }
        $closure($this->params);
        exit(0);
    }

}