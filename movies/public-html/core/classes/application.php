<?php

namespace classes;

use League\OAuth2\Client\Token\AccessToken;

class application
{
    /**
     * @var null | \classes\application
     */
    protected static $app = null;

    /**
     * @var null|\OAuth2\Server
     */
    var $auth = null;
    var $authProvider = null;

    private static $db = null;

    private function __construct()
    {
        $this->authProvider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId' => config::$clientId,
            'clientSecret' => config::$clientSecret,
            'urlAuthorize' => config::$authServer . '/auth',
            'urlAccessToken' => config::$authServer . '/access_token',
            'urlResourceOwnerDetails' => config::$authServer . '/owner_details',
        ]);
        $this->firstRun();
    }

    /**
     * @return null|\PDO
     */
    public function db()
    {
        if (is_null(static::$db)) {
            static::$db = new \PDO('sqlite:' . config::$app_path . 'data/mydb.sq3');
        }
        return static::$db;
    }


    protected function firstRun()
    {
        $first = false;
        if (!is_dir(config::$app_path . 'data')) {
            if (!mkdir(config::$app_path . 'data/', 0777, true)) {
                throw new \Exception('Could not create data dir');
            }
            $first = true;
        }

        if ($first) {
            $f = file_get_contents(config::$app_path . 'migration/tables.txt');
            preg_match_all('#(CREATE TABLE .*?;)#s', $f, $preg);
            foreach ($preg[1] as $sql) {
                $this->db()->query($sql);
            }
            $f = fopen(config::$app_path . 'migration/movies.txt', 'r');
            while (!feof($f)) {
                $line = trim(fgets($f));
                list($title, $description, $year) = explode(';', $line);
                $title = trim(htmlentities($title, ENT_QUOTES, 'utf-8', false));
                $description = trim(htmlentities($description, ENT_QUOTES, 'utf-8', false));
                $year = intval($year);
                if (is_numeric($year)) {
                    $res = $this->db()->query('insert into movies(title,description,created_at, added_at, added_by)
                    VALUES ("' . $title . '","' . $description . '","' . $year . '","' . date('Y-m-d H:i:s') . '", 0)');
                    if (!$res) {
                        print_r($this->db()->errorInfo());
                        die();
                    }
                }
            }
        }
    }


    public function getAuthId($params)
    {
        $token = new AccessToken(['access_token' => $params['token']]);

        try {
            $request = $this->authProvider->getAuthenticatedRequest(
                'GET', 'http://auth/owner_details?token=' . $token->getToken() . '&nc=' . time(), $token
            );
            $request = $this->authProvider->getParsedResponse($request);

            if (!isset($request['user_id'])) {
                return new \Exception('Cannot check the token');
            }

            return $request['user_id'];
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function app()
    {
        if (!is_null(self::$app)) {
            return self::$app;
        }
        return self::$app = new self;
    }

    public function returnJson($result, $message, $data = [])
    {
        header('Content-type: application/json');
        echo json_encode(['result' => $result, 'message' => $message, 'data' => $data]);
        exit(0);
    }


}