<?php

namespace classes;


class config
{
    static $app_path = '';

    static $clientId = 'demoApp';
    static $clientSecret = 'demoAppPass';
    static $authServer = 'http://auth';

    static function init()
    {
        static::$app_path = realpath(__DIR__ . '/../../') . '/';
    }
}