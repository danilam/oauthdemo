<?php

namespace classes;


class template
{
    static function render($container, $include = '', $vars = [])
    {
        $container = config::$app_path . 'core/views/' . $container . '.php';
        if (!file_exists($container)) {
            throw new \Exception('Template ' . $container . ' not found');
        }
        if ($include) {
            $include = config::$app_path . 'core/views/' . $include . '.php';
        }
        global $vars;
        require($container);
    }
}