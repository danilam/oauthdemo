function spinStart(obj) {
    $(obj).addClass('disabled').removeClass('btn-success');
    return $(obj);
}

function spinStop(obj) {
    $(obj).removeClass('disabled').addClass('btn-success');
}

$(function () {

    //auth servers
    $('#ex1 .span-links span').click(function () {
        var tmp = $(this).html()
        tmp = tmp.split(';')
        $('#ex1 [name=username]').val(tmp[0]);
        $('#ex1 [name=password]').val(tmp[1]);
    });

    $('#ex1 .btn').click(function () {
        obj = spinStart(this)
        var username = $('#ex1 [name=username]').val();
        var password = $('#ex1 [name=password]').val();
        $.ajax({
            url: '/ajax',
            type: 'POST',
            data: {
                method: 'auth_get_token',
                username: username,
                password: password
            },
            dataType: 'json',
            success: function (data) {
                spinStop(obj)
                if (data.result === 'ok') {
                    $('#ex1 .result-text').val(data.data.token.access_token)
                    $('.token').val(data.data.token.access_token)
                    $('#ex1 .result-raw').val(JSON.stringify(data.data))
                    $('#userid').html(username);
                } else {
                    $('#ex1 .result-text').val('error')
                    $('#ex1 .result-raw').val(JSON.stringify(data.data))
                }
            }
        });
    });


    $('#mov3 .btn').click(function () {
        obj = spinStart(this)

        $('#result_table').html('');
        $.ajax({
            url: '/ajax',
            type: 'POST',
            data: {
                method: 'mov_get_list',
                token: $('#token').val()
            },
            dataType: 'json',
            success: function (data) {
                spinStop(obj)
                if (data.result === 'ok') {
                    var html = [];
                    $('#mov3 .result-raw').val(JSON.stringify(data.message))
                    $(data.data).each(function (a, b) {
                        var tmp = '<tr>';
                        tmp += '<td>' + b.movie_id + '</td>';
                        tmp += '<td>' + b.title + '</td>';
                        tmp += '<td>' + b.description + '</td>';
                        tmp += '<td>' + b.created_at + '</td>';
                        tmp += '<td>' + b.added_by + '</td>';
                        tmp += '<td>' + b.added_at + '</td>';
                        tmp += '</tr>';
                        html.push(tmp);
                    });

                    $('#result_table').html(html.join("\n"));
                } else {
                    $('#mov3 .result-raw').val(JSON.stringify(data.message))
                }
            }
        });
    });
    $('#mov2 .btn').click(function () {
        obj = spinStart(this)

        $('#result_table').html('');
        $.ajax({
            url: '/ajax',
            type: 'POST',
            data: {
                method: 'mov_add_fav',
                movie_id: $('#mov2 [name=id]').val(),
                token: $('#token').val()
            },
            dataType: 'json',
            success: function (data) {
                spinStop(obj)
                if (data.result === 'ok') {
                    $('#mov2 .result-raw').val(JSON.stringify(data.message))
                } else {
                    $('#mov2 .result-raw').val(JSON.stringify(data.message))
                }
            }
        });
    });
    $('#mov1 .btn').click(function () {
        obj = spinStart(this)
        $('#result_table').html('');

        $.ajax({
            url: '/ajax',
            type: 'POST',
            data: {
                method: 'mov_add_movie',
                title: $('#mov1 [name=title]').val(),
                description: $('#mov1 [name=description]').val(),
                year: $('#mov1 [name=year]').val(),
                token: $('#token').val()
            },
            dataType: 'json',
            success: function (data) {
                spinStop(obj)
                if (data.result === 'ok') {
                    $('#mov1 .result-raw').val(JSON.stringify(data.data))
                } else {
                    $('#mov1 .result-raw').val(data.message)
                }
            }
        });
    });


});