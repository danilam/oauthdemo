Тестовое RESTful API
=========================

Настройки
-------------------------------

###Даные пользователей

./auth/public-html/migration/users.txt
Формат: login;password


###Данные по фильмам

./movies/public-html/migration/movies.txt
Формат: title;description;year


Запуск
----------------------------
sudo ./run.sh

Сброс данных
---------------
sudo ./clear.sh

Веб-Интерфейс к API
-----------------------------
http://localhost:8089


Авторизация на сервере авторизации
-----------------------

Метод POST

Адрес: http://localhost:8088/access_token

Параметры: 
* username
* password
* application_id = demoApp
* application_secret = demoAppPass
* grant_type = password

curl -u demoApp:demoAppPass http://localhost:8088/access_token -d "username=1&password=2&grant_type=password"

{"error":"invalid_grant","error_description":"Invalid username and password combination"}

curl -u demoApp:demoAppPass http://localhost:8088/access_token -d "username=test1&password=test1pass&grant_type=password"


{"access_token":"8eeb12e01c8cac683e3cc95ad5cbe615dc53724b","expires_in":3600,"token_type":"Bearer","scope":null,"refresh_token":"99afa393df68b0e29d72f7327d76d8f9049b4ae0"}



Авторизация на приложении
---------------------------

Метод POST

Адрес: http://localhost:8089/ajax

Параметры: 
* method = auth_get_token
* username
* password

curl http://localhost:8089/ajax -d "method=auth_get_token&username=test1&password=test1pass"

{"result":"ok","message":"success","data":{"token":{"token_type":"Bearer","scope":null,"access_token":"c28af1808e840f35996f6fe2b5480b1dd8cbf3c9","refresh_token":"c01574e62fc8b4104597f15ab1769a8a649681c3","expires":1540469724}}}


Добавить фильм
-------------------------

Метод POST

Адрес: http://localhost:8089/ajax

Параметры: 
* method = mov_add_movie
* token
* title
* description
* year

curl http://localhost:8089/ajax -d "method=mov_add_movie&token=XXX&title=zzz&description=nnn&year=123"

{"result":"ok","message":"success","data":{"id":"ID добавленного фильма"}}


Добавить фильм в избранное
-----------------------------------

Метод POST

Адрес: http://localhost:8089/ajax

Параметры: 
* method = mov_add_fav
* movie_id 

curl http://localhost:8089/ajax -d "method=mov_add_fav&token=XXX&movie_id=1"

{"result":"ok","message":"ok","data":[]}


Список фильмов
-------------------------------------

Метод POST

Адрес: http://localhost:8089/ajax
* Параметры: 
* method = mov_get_list


curl http://localhost:8089/ajax -d "method=mov_get_list&token=XXX"

{"result":"ok","message":"ok","data":[]}
в data будет массив с фильмами


