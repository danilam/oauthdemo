<?php

namespace classes;

use \OAuth2;

class auth_storage extends OAuth2\Storage\Pdo
{

    public $db;

    public function __construct($connection, array $config = array())
    {
        $first = false;
        if (!is_dir(config::$app_path . 'data/')) {
            if (!mkdir(config::$app_path . 'data/', 0777, true)) {
                throw new \Exception('Could not create data dir');
            }
            $first = true;
        }
        parent::__construct($connection, $config);

        if ($first) {

            $f = file_get_contents(config::$app_path . 'migration/tables.txt');
            preg_match_all('#(CREATE TABLE .*?;)#s', $f, $preg);
            foreach ($preg[1] as $sql) {
                $this->db->query($sql);
            }

            $fs = fopen(config::$app_path . 'migration/users.txt', 'r');
            while (!feof($fs)) {
                $line = trim(fgets($fs));
                list($login, $password) = explode(';', $line);
                if ($login && $password) {
                    $this->setUser($login, $password);
                }
            }

            $this->setClientDetails('demoApp', 'demoAppPass');


        }


    }


}