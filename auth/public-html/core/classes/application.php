<?php

namespace classes;

use \OAuth2;

class application
{
    /**
     * @var null | \classes\application
     */
    protected static $app = null;
    /**
     * @var \classes\template|null
     */
    var $template = null;

    /**
     * @var null|\OAuth2\Server
     */
    var $auth = null;

    /**
     * @var null | \classes\auth_storage
     */
    var $storage = null;

    private function initializeAuthStorage()
    {

        $this->storage = new auth_storage(['dsn' => 'sqlite:' . config::$app_path . 'data/mydb.sq3']);
        $this->auth = new OAuth2\Server($this->storage);
        $this->auth->addGrantType(new OAuth2\GrantType\UserCredentials($this->storage)); // or any grant type you like!
        $this->auth->addGrantType(new OAuth2\GrantType\AuthorizationCode($this->storage)); // or any grant type you like!
    }

    private function __construct()
    {

        $this->initializeAuthStorage();

    }

    public static function app()
    {
        if (!is_null(self::$app)) {
            return self::$app;
        }
        return self::$app = new self;
    }


}