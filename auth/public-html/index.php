<?php
chdir(__DIR__);
require_once('./loader.php');
spl_autoload_register(function ($class) {
    $path = explode('\\', $class);
    if (!in_array($path[0], ['classes', 'views', 'models'])) {
        throw new \Exception('Access violation');
    }
    $filename = './core/' . join('/', $path) . '.php';
    if (!file_exists($filename)) {
        throw new \Exception('Class not ' . $class . ' found');
    }

    require_once($filename);

});


\classes\config::init();
$router = new \classes\router();

$router->post('/access_token', function () {
    \classes\application::app()->auth->handleTokenRequest(\OAuth2\Request::createFromGlobals())->send();
});

$router->get('/owner_details', function () {
    if (!\classes\application::app()->auth->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
        \classes\application::app()->auth->getResponse()->send();
        exit(0);
    }
    $token = \classes\application::app()->auth->getAccessTokenData(OAuth2\Request::createFromGlobals());
    $res = \classes\application::app()->storage->db->query('SELECT id from oauth_users where username="' . $token['user_id'] . '" limit 1');
    $user_data = $res->fetch();
    echo json_encode(['result' => true, 'user_id' => $user_data['id'], 'expires' => $token['expires']]);
});

$router->post('/auth', function () {
    if (!\classes\application::app()->auth->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
        \classes\application::app()->auth->getResponse()->send();
        exit(0);
    }
    $token = \classes\application::app()->auth->getAccessTokenData(OAuth2\Request::createFromGlobals());
    echo json_encode(array('success' => true, 'message' => 'success', 'data' => [
        'user_id' => $token['user_id'],
        'expires' => $token['expires']
    ]));
});